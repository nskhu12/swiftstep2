package com.sweeftdigital;

import java.util.HashMap;
import java.util.Map;

class CustomDataStructure<T> {
    private Map<T, Node<T>> hashMap;
    private Node<T> head;
    private Node<T> tail;

    private static class Node<T> {
        T data;
        Node<T> prev;
        Node<T> next;

        Node(T data) {
            this.data = data;
        }
    }

    public CustomDataStructure() {
        hashMap = new HashMap<>();
        head = new Node<>(null); // Dummy head node
        tail = new Node<>(null); // Dummy tail node
        head.next = tail;
        tail.prev = head;
    }

    public void insert(T data) {
        if (!hashMap.containsKey(data)) {
            Node<T> newNode = new Node<>(data);
            hashMap.put(data, newNode);
            addToTail(newNode);
        }
    }

    public void delete(T data) {
        if (hashMap.containsKey(data)) {
            Node<T> nodeToDelete = hashMap.get(data);
            removeNode(nodeToDelete);
            hashMap.remove(data);
        }
    }

    private void addToTail(Node<T> node) {
        node.prev = tail.prev;
        tail.prev.next = node;
        node.next = tail;
        tail.prev = node;
    }

    private void removeNode(Node<T> node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    public void printList() {
        Node<T> current = head.next;
        while (current != tail) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }

    public String getListAsString() {
        StringBuilder sb = new StringBuilder();
        Node<T> current = head.next;

        while (current != tail) {
            sb.append(current.data).append(" ");
            current = current.next;
        }

        // Remove the trailing space, if any
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }

        return sb.toString();
    }
}
