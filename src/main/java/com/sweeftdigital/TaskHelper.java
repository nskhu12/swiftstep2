package com.sweeftdigital;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class TaskHelper {

    private TaskHelper() {

    }

    // Task 1
    public static int singleNumber(int[] nums) {

        List<Integer> seen = IntStream.of(nums)
                .boxed()
                .toList();

        for (int num : nums) {
            if (seen.indexOf(num) == seen.lastIndexOf(num)) {
                return num;
            }
        }

        return -1;
    }

    // Task 2
    public static int minSplit(int amount) {
        int[] coins = {50, 20, 10, 5, 1};

        int minCoins = 0;

        for (int coin : coins) {
            if (amount >= coin) {
                minCoins += amount / coin;
                amount %= coin;
            }
        }

        return minCoins;
    }

    // Task 3
    public static int notContains(int[] array) {
        Arrays.sort(array);

        int smallestMissing = 1;

        for (int num : array) {
            if (num == smallestMissing) {
                smallestMissing++;
            } else if (num > smallestMissing) {
                return smallestMissing;
            }
        }

        return smallestMissing;
    }

    // Task4
    public static String addBinary(String a, String b) {
        BigInteger num1 = new BigInteger(a, 2);
        BigInteger num2 = new BigInteger(b, 2);
        BigInteger sum = num1.add(num2);

        return sum.toString(2);
    }

    // Task 5
    public static int countVariants(int stairsCount) {
        if (stairsCount <= 1) {
            return 1;
        }

        int[] dp = new int[stairsCount + 1];
        dp[0] = 1;
        dp[1] = 1;

        for (int i = 2; i <= stairsCount; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }

        return dp[stairsCount];
    }
}
