package com.sweeftdigital;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CustomDataStructureTest {

    private CustomDataStructure<Integer> customDS;

    @BeforeEach
    public void setUp() {
        customDS = new CustomDataStructure<>();
    }

    @Test
    public void testInsert() {
        customDS.insert(1);
        customDS.insert(2);
        customDS.insert(3);

        customDS.printList(); // Expected: 1 2 3
        Assertions.assertEquals("1 2 3", customDS.getListAsString());
    }

    @Test
    public void testInsertDuplicates() {
        customDS.insert(1);
        customDS.insert(2);
        customDS.insert(1);

        customDS.printList(); // Expected: 1 2
        Assertions.assertEquals("1 2", customDS.getListAsString());
    }

    @Test
    public void testDelete() {
        customDS.insert(1);
        customDS.insert(2);
        customDS.insert(3);

        customDS.delete(2);

        customDS.printList(); // Expected: 1 3
        Assertions.assertEquals("1 3", customDS.getListAsString());
    }

    @Test
    public void testDeleteNonExistentElement() {
        customDS.insert(1);
        customDS.insert(2);
        customDS.insert(3);

        customDS.delete(4);

        customDS.printList(); // Expected: 1 2 3
        Assertions.assertEquals("1 2 3", customDS.getListAsString());
    }

    @Test
    public void testDeleteAllElements() {
        customDS.insert(1);
        customDS.insert(2);
        customDS.insert(3);

        customDS.delete(1);
        customDS.delete(2);
        customDS.delete(3);

        customDS.printList(); // Expected: (empty list)
        Assertions.assertEquals("", customDS.getListAsString());
    }

    @Test
    public void testDeleteDuplicates() {
        customDS.insert(1);
        customDS.insert(2);
        customDS.insert(1);

        customDS.delete(1);

        customDS.printList(); // Expected: 2
        Assertions.assertEquals("2", customDS.getListAsString());
    }
}
