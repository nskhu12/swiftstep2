package com.sweeftdigital;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TaskHelperTest {

    // Task 1: Find the Unique Number in an Array
    @Test
    public void testSingleNumber() {
        int[] nums1 = {2, 2, 1, 3, 3};
        int result1 = TaskHelper.singleNumber(nums1);
        Assertions.assertEquals(1, result1);

        int[] nums2 = {4, 4, 5, 5, 6};
        int result2 = TaskHelper.singleNumber(nums2);
        Assertions.assertEquals(6, result2);

        int[] nums3 = {1, 1, -1, -1, 0};
        int result3 = TaskHelper.singleNumber(nums3);
        Assertions.assertEquals(0, result3);

        int[] nums4 = {1, 3, 3};
        int result4 = TaskHelper.singleNumber(nums4);
        Assertions.assertEquals(1, result4);

        int[] nums5 = {1, 3, 3, 3};
        int result5 = TaskHelper.singleNumber(nums5);
        Assertions.assertEquals(1, result5);
    }

    // Task 2: Minimum Coins for a Given Amount
    @Test
    public void testMinSplit() {
        int amount1 = 63;
        int result1 = TaskHelper.minSplit(amount1);
        Assertions.assertEquals(5, result1);

        int amount2 = 123;
        int result2 = TaskHelper.minSplit(amount2);
        Assertions.assertEquals(6, result2);

        int amount3 = 1;
        int result3 = TaskHelper.minSplit(amount3);
        Assertions.assertEquals(1, result3);

        int amount4 = 5;
        int result4 = TaskHelper.minSplit(amount4);
        Assertions.assertEquals(1, result4);

        int amount5 = 10;
        int result5 = TaskHelper.minSplit(amount5);
        Assertions.assertEquals(1, result5);

        int amount6 = 20;
        int result6 = TaskHelper.minSplit(amount6);
        Assertions.assertEquals(1, result6);

        int amount7 = 50;
        int result7 = TaskHelper.minSplit(amount7);
        Assertions.assertEquals(1, result7);
    }

    // Task 3: Find the Minimum Integer Not in Array
    @Test
    public void testNotContains() {
        int[] array1 = {3, 4, -1, 1, 6, 2};
        int result1 = TaskHelper.notContains(array1);
        Assertions.assertEquals(5, result1);

        int[] array2 = {-3, 1, -2, 4};
        int result2 = TaskHelper.notContains(array2);
        Assertions.assertEquals(2, result2);

        int[] array3 = {1, 1, 3, 4};
        int result3 = TaskHelper.notContains(array3);
        Assertions.assertEquals(2, result3);

        int[] array4 = {-1, -2};
        int result4 = TaskHelper.notContains(array4);
        Assertions.assertEquals(1, result4);

        int[] array5 = {1};
        int result5 = TaskHelper.notContains(array5);
        Assertions.assertEquals(2, result5);
    }

    // Task 4: Binary String Addition
    @Test
    public void testAddBinary() {
        String a1 = "1010";
        String b1 = "1011";
        String result1 = TaskHelper.addBinary(a1, b1);
        Assertions.assertEquals("10101", result1);

        String a2 = "111";
        String b2 = "111";
        String result2 = TaskHelper.addBinary(a2, b2);
        Assertions.assertEquals("1110", result2);

        String a3 = "1101";
        String b3 = "100";
        String result3 = TaskHelper.addBinary(a3, b3);
        Assertions.assertEquals("10001", result3);
    }

    // Task 5: Count Variants to Climb Stairs
    @Test
    public void testCountVariants() {
        int stairsCount1 = 4;
        int result1 = TaskHelper.countVariants(stairsCount1);
        Assertions.assertEquals(5, result1);

        int stairsCount2 = 1;
        int result2 = TaskHelper.countVariants(stairsCount2);
        Assertions.assertEquals(1, result2);

        int stairsCount3 = 0;
        int result3 = TaskHelper.countVariants(stairsCount3);
        Assertions.assertEquals(1, result3);
    }
}
