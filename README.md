# Task Descriptions

### Task 1: Find the Unique Number in an Array

Given an array of integers where every number except one is duplicated, find the unique number.

### Task 2: Minimum Coins for a Given Amount

Find the minimum number of coins needed to represent a given integer amount using specified coin denominations.

### Task 3: Find the Minimum Integer Not in Array

Write a method to find the minimum integer number (>0) that is not contained in a given array of integers.

### Task 4: Binary String Addition

Given two binary strings, return their sum as a binary string.

### Task 5: Count Variations to Climb Stairs

Count the number of different variations to climb a staircase with 'n' steps, where you can take 1 or 2 steps in one move.

### Task 6: Custom Data Structure with O(1) Element Deletion

Create a custom data structure that allows O(1) element deletion while maintaining efficient insertion and lookup operations.
